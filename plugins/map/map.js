// *
// * Add multiple markers
// * 2013 - en.marnoto.com
// *

// necessary variables
var map;
var infoWindow;

// markersData variable stores the information necessary to each marker
var markersData = [
   {
      lat:            40.6386333,
      lng:            -8.745,
      title:          "Australia",
      flagSrc:        "images/icon/australia.png",
      gallery:        "images/map_gallery.jpg",
      ticketLink:     "#", 
      ticketImg:      "images/icon/ticket.png",
      ticketText:     "Search tour",
      shareLink1:     "#",
      shareLink2:     "#",
      shareLink3:     "#",
      shareLink1Text: "784",
      shareLink2Text: "12",
      shareLink3Text: "1200",
      markerText:     "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when",
      markerIcon :    "plugins/map/marker1.png"
   },
   {
      lat:            40.6386333,
      lng:            -20.745,
      title:          "Ukraine",
      flagSrc:        "images/icon/australia.png",
      gallery:        "images/map_gallery.jpg",
      ticketLink:     "#", 
      ticketImg:      "images/icon/ticket.png",
      ticketText:     "Search tour",
      shareLink1:     "#",
      shareLink2:     "#",
      shareLink3:     "#",
      shareLink1Text: "704",
      shareLink2Text: "12",
      shareLink3Text: "300",
      markerText:     "Lorem Ipsum is simply dummy text of the printing and ",
      markerIcon :    "plugins/map/marker2.png"
   },
   {
      lat:            40.6386333,
      lng:            -40.745,
      title:          "USA",
      flagSrc:        "images/icon/australia.png",
      gallery:        "images/map_gallery.jpg",
      ticketLink:     "#", 
      ticketImg:      "images/icon/ticket.png",
      ticketText:     "Search tour",
      shareLink1:     "#",
      shareLink2:     "#",
      shareLink3:     "#",
      shareLink1Text: "904",
      shareLink2Text: "12",
      shareLink3Text: "1250",
      markerText:     "Lorem Ipsum is simply ",
      markerIcon :    "plugins/map/marker3.png"
   } 
];


function initialize() {
   var mapOptions = {
      center: new google.maps.LatLng(50.84757295,-21.00585937),
      zoom: 2,
      panControl: false,
      mapTypeId: 'roadmap',
      scrollwheel: false,
      mapTypeControl: false,
      controlPosition: true,
      controlPositionOptions : {
        position: google.maps.ControlPosition.TOP_CENTER
      },
      scaleControl: true,
      streetViewControl: true,
      streetViewControlOptions: {
         position: google.maps.ControlPosition.TOP_LEFT
      },
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      }
   };

   map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

   // a new Info Window is created
   infoWindow = new google.maps.InfoWindow({
      pixelOffset: new google.maps.Size(182, 211),
   });

   // Event that closes the Info Window with a click on the map
   google.maps.event.addListener(map, 'click', function() {
      infoWindow.close();
   });

   // Finally displayMarkers() function is called to begin the markers creation
   displayMarkers();

}
google.maps.event.addDomListener(window, 'load', initialize);


// This function will iterate over markersData array
// creating markers with createMarker function
function displayMarkers(){

   // this variable sets the map bounds according to markers position
   var bounds = new google.maps.LatLngBounds();
   
   // for loop traverses markersData array calling createMarker function for each marker 
   for (var i = 0; i < markersData.length; i++){

      var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);

      var title          = markersData[i].title;
      var flagSrc        = markersData[i].flagSrc ;
      var gallery        = markersData[i].gallery ;
      var ticketLink     = markersData[i].ticketLink ;
      var ticketImg      = markersData[i].ticketImg ;
      var ticketText     = markersData[i].ticketText ;
      var shareLink1     = markersData[i].shareLink1 ;
      var shareLink2     = markersData[i].shareLink2 ;
      var shareLink3     = markersData[i].shareLink3 ;
      var shareLink1Text = markersData[i].shareLink1Text ;
      var shareLink2Text = markersData[i].shareLink2Text ;
      var shareLink3Text = markersData[i].shareLink3Text ;
      var markerText     = markersData[i].markerText ;
      var markerIcon     = markersData[i].markerIcon ;

      createMarker(latlng , title , flagSrc , gallery , ticketLink , ticketImg , ticketText , shareLink1 , shareLink2 , shareLink3 , shareLink1Text , shareLink2Text , shareLink3Text , markerText ,markerIcon );

      // marker position is added to bounds variable
      bounds.extend(latlng);  
   }

   // Finally the bounds variable is used to set the map bounds
   // with fitBounds() function
   map.fitBounds(bounds);

}

// This function creates each marker and it sets their Info Window content
function createMarker(latlng, title , flagSrc , gallery , ticketLink , ticketImg , ticketText , shareLink1 , shareLink2 , shareLink3 , shareLink1Text , shareLink2Text , shareLink3Text , markerText , markerIcon ){
   var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      title: name,
      icon: markerIcon 
   });

   // This event expects a click on a marker
   // When this event is fired the Info Window content is created
   // and the Info Window is opened.
   google.maps.event.addListener(marker, 'click', function() {
      
      // Creating the content to be inserted in the infowindow

var iwContent =  '<div class="map_marker_box">' +

            '<div class="map_header_marker clearfix">' +
               '<span class="map_flag">' +
                  '<img src="' + flagSrc + '">'+
               '</span>'+
               '<h2 class="map_title">' + title + '</h2>'+
               '<a href="#" class="map_full_screen">'+
                  '<img src="images/icon/fullscreen.png">'+
               '</a>'+
            '</div>'+

            '<div class="map_gallery clearfix">' +
               '<img src=" '+ gallery +' ">' +
               '<div class="map_img_abs"><img src=" ' + markerIcon + ' "></div>' +
            '</div>' +

            '<div class="map_footer_marker clearfix">' +

                '<div class="map_footer_marker_left">' +
                    '<a class="map_footer_marker_left_link" href=" ' + ticketLink + ' ">' +
                        '<span>' +
                            '<img src=" '+ ticketImg +' " alt=""><br>' +
                            '<span> ' + ticketText + ' </span>' +
                        '</span>' +
                    '</a>' +
                '</div>' +

                '<div class="map_footer_marker_right">' +
                    '<div class="map_footer_marker_right_links">' +
                        '<a href=" ' + shareLink1 + ' " class="btn_light" data-icon="eye"> ' + shareLink1Text + ' </a>' +
                        '<a href=" ' + shareLink2 + ' " class="btn_light" data-icon="icon"> ' + shareLink2Text + ' </a>' +
                        '<a href=" ' + shareLink3 + ' " class="btn_light" data-icon="photo"> ' + shareLink3Text + ' </a>' +
                    '</div>' +
                    '<div class="map_footer_marker_right_text"> ' + markerText +  '</div>' +
                '</div>' +

            '</div></div>';
      
      // including content to the Info Window.
      infoWindow.setContent(iwContent);

      // opening the Info Window in the current map and at the current marker location.
      infoWindow.open(map, marker);
   });
}