// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ СТАРТ
		
		// $(function(){
		// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
		// 	ua = navigator.userAgent,

		// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

		// 	scaleFix = function () {
		// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
		// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
		// 			document.addEventListener("gesturestart", gestureStart, false);
		// 		}
		// 	};
			
		// 	scaleFix();
		// });
		// var ua=navigator.userAgent.toLocaleLowerCase(),
		//  regV = /ipod|ipad|iphone/gi,
		//  result = ua.match(regV),
		//  userScale="";
		// if(!result){
		//  userScale=",user-scalable=0"
		// }
		// document.write('<meta name="viewport" id="myViewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

		// ============================================================
		//  window.onload = function () {
		// 	if(screen.width <= 617) {
		// 	    var mvp = document.getElementById('myViewport');
		// 	    mvp.setAttribute('content','width=617');
		// 	}
		// }
		// ============================================================

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ КОНЕЦ




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var tabs         = $('.tabs'),
		    styler       = $(".styler"),
			datepicker   = $(".datepicker"),
			mapCanvas    = $("#map-canvas"),
			rating       = $(".rating"),
			popup        = $("[data-popup]"),
			tooltip      = $(".tooltip"),
			accordion    = $(".accordion"),
			rangeSlider  = $("#slider-range"),
			windowW      = $(window).width(),
			windowH      = $(window).height();


			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(tabs.length){
					include("plugins/easy-responsive-tabs/easyResponsiveTabs.js");
			}
			if(datepicker.length){
					include('plugins/datepicker/jquery-ui-datepicker.js');
					// include('plugins/datepicker/datepicker-ru.js');
			}
			if(mapCanvas.length){
					include('https://maps.googleapis.com/maps/api/js?sensor=false');
					include('plugins/map/map.js');
			}
			if(rating.length){
					include("plugins/rating/jquery.raty.js");
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}
			if(tooltip.length){
					include("plugins/tooltip/jquery.tooltipster.js");
			}
			if(accordion.length){
					include("plugins/accordion/jquery-ui-accordion.js");
			}
			if(rangeSlider.length){
					include("plugins/range-slider/jquery-ui-slider.js");
			}

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */




			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.easyResponsiveTabs();
					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */




			/* ------------------------------------------------
			DATAPICKER START
			------------------------------------------------ */

					if(datepicker.length){
						$(".datepicker").datepicker({
					        dateFormat: "d.mm.yy",
					        minDate: new Date()
					    });
					}

			/* ------------------------------------------------
			DATAPICKER END
			------------------------------------------------ */




			/* ------------------------------------------------
			RATING START
			------------------------------------------------ */
					if (rating.length){

				        rating.each(function(){
				         	var itemStar = $(this).attr("data-star");
				          	if(!$(this).hasClass('staticStar')){
				            	$(this).raty({
				              		score: function() {
				                	return $(this).attr('data-star');
				              		}
				            	});
				          	}
				          	else{
				              	$(this).raty({ readOnly: true, score: itemStar });    
				        	}
				        });

				    }
		    /* ------------------------------------------------
			RATING END
			------------------------------------------------ */




			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){

						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal({
						    	afterClose: function(){
						    		if(modal == '#log-in'){
										$('body').addClass('close_popup'); 
										activeBtnPopup();
						    		}
						    	}
						    });

						    if(modal == '#log-in'){
							    $(this).addClass('active');
							    $('body').removeClass('close_popup'); 

							    function activeBtnPopup(){
									if($('body').hasClass('close_popup')){
								    	popup.removeClass('active');
								    }
								}
								activeBtnPopup();

							}
						});
						
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */




			/* ------------------------------------------------
			TOOLTIP START
			------------------------------------------------ */

					if(tooltip.length){

						tooltip.tooltipster({
							maxWidth: 183,
							position : 'bottom',
						});

					}

			/* ------------------------------------------------
			TOOLTIP END
			------------------------------------------------ */



			/* ------------------------------------------------
			ACCORDION START
			------------------------------------------------ */

					if(accordion.length){

						accordion.accordion({
						    collapsible: true
						});
						
					}

			/* ------------------------------------------------
			ACCORDION END
			------------------------------------------------ */




			/* ------------------------------------------------
			RANGE-SLIDER START
			------------------------------------------------ */

					if(rangeSlider.length){

						function convert(seconds){

							var res,
								h = parseInt( seconds / 3600 ) % 24,
								m = parseInt( seconds / 60 ) % 60;

							return (h.toString().length == 1 ? '0' + h : h ) + ":" + (m.toString().length == 1 ? '0' + m : m);
						}

						rangeSlider.slider({
							range: true,
							min: 0,
							max: 86400,
							values: [ 21600, 54000 ],
							slide: function( event, ui ) {
							   	$( "#amount" ).val( convert(ui.values[ 0 ])  + " - " + convert(ui.values[ 1 ]) );
							},
							create: function(event, ui){
								var values = rangeSlider.slider("values");

								$( "#amount" ).val(convert(values[0]) + " - " + convert(values[ 1 ]));
							}
						});

						// $( "#amount" ).val( "$" + $(rangeSlider).slider( "values", 0 ) + " - $" + $(rangeSlider).slider( "values", 1 ) );

					}

			/* ------------------------------------------------
			RANGE-SLIDER END
			------------------------------------------------ */




		});

		
		$(window).load(function(){

			

		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
